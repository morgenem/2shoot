//Input class
//SFML specific input handling.

#ifndef INPUT_H
#define INPUT_H

#include "vec.h"
#include "graphics/window.h"

class InputEngine
{
    bool keyboard_w = false;
    bool keyboard_a = false;
    bool keyboard_s = false;
    bool keyboard_d = false;
    Vec<int,2> mouse_pos; //sfml mouse position is int, so it's int here.
    // Maybe change this later so all Vecs are float vecs?
    const Window &window_ref;
public:
    InputEngine(const Window &window);
    void update();

    bool get_keyboard_w() const;
    bool get_keyboard_a() const;
    bool get_keyboard_s() const;
    bool get_keyboard_d() const;
    Vec<int,2> get_mouse_pos() const;
};

#endif
