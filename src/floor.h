//Floor class.
//Floors are rectangle objects.
#ifndef FLOOR_H
#define FLOOR_H

#include "component/position_component.h"
#include "component/rotation_component.h"
#include "component/size_component.h"
#include "vec.h"
#include "typedef.h"
#include "rectangle.h"

class Floor
{
public:
    Rectangle rect;
    Floor(Rectangle r);
};

#endif
