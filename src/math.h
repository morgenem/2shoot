#ifndef MATH_H
#define MATH_H

#include <cmath>
#include "vec.h"
#include <iostream>

namespace Math{
    inline float round_insig(float x) //Round to zero for any unimportant decimals
    {
        if(x < 0.01 && x > 0)
            return 0;
        if(x > -0.01 && x < 0)
            return 0;
        else
            return x;
    }

    inline float deg_to_rad(float deg)
    {
        float rad = deg * (M_PI/180);
        if(rad < 0.01 && rad > 0)
            return 0;
        if(rad > -1 && rad < 0)
            return 0;
        return rad;
    }

    inline float rad_to_deg(float rad)
    {
        float deg = rad * (180/M_PI);
        if(round_insig(rad-M_PI) == 0){
            return 180;
        }
        if(deg < 0.01 && deg > 0)
            return 0;
        if(deg > -0.01 && deg < 0)
            return 0;
        return deg;
    }

    inline float cos_component(Vec<float, 2> vec) //Return cosin component of given Vec
    {
        return vec[0] * std::cos(deg_to_rad(vec[1]));
    }
    inline float sin_component(Vec<float, 2> vec)
    {
        return vec[0] * std::sin(deg_to_rad(vec[1]));
    }

    inline float abs(float x)
    {
        if(x < 0.0)
            return -x;
        else
            return x;
    }
}

#endif
