#include "player.h"

Player::Player(PosVec pos, SizeVec size_vec, float mass)
: position_component(pos), velocity(5.0f/60.0f),
player_controller_component(position_component, rigid_body_component, velocity),
rotation_component(90), rigid_body_component(&position_component, size_vec)
{

}

void Player::update()
{
    rigid_body_component.update();
}
