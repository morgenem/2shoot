#include "rectangle.h"

Rectangle::Rectangle(PosVec lt, PosVec rt, PosVec lb, PosVec rb)
:   ltop(lt), rtop(rt), lbot(lb), rbot(rb)
{
    float length = rtop[0] - ltop[0];
    if(length < 0.0)
        length = -length;

    float height = rtop[1] - rbot[1];
    if(height < 0.0)
        height = -height;

    SizeVec size_vec({length,height});
    size_component.set_size(size_vec);
}

Rectangle::Rectangle()
{

}

PosVec Rectangle::get_center() const
{
    float cx = ltop[0] + size_component.get_size()[0]/2;
    float cy = lbot[1] + size_component.get_size()[1]/2;
    return PosVec({cx,cy});
}

Rectangle Rectangle::from_two_corners(PosVec lt, PosVec rb)
{
    float length = rb[0] - lt[0];
    if(length < 0.0)
        length = -length;

    float height = lt[1] - rb[1];
    if(height < 0.0)
        height = -height;

    PosVec lb = PosVec({lt[0], lt[1]-height});
    PosVec rt = PosVec({rb[0], rb[1]+height});

    return Rectangle(lt, rt, lb, rb);
}

Rectangle Rectangle::from_center(PosVec center, SizeVec size_vec)
{
    float ltx = center[0] - (size_vec[0]/2.0);
    float lty = center[1] + (size_vec[1]/2.0);
    PosVec lt({ltx,lty});
    float rbx = center[0] + (size_vec[0]/2.0);
    float rby = center[1] - (size_vec[1]/2.0);
    PosVec rb({rbx,rby});

    return from_two_corners(lt,rb);
}
