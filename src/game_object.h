//GameObject Class
//All game objects (such as wall and enemy) inherit from this class

#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "component/position_component.h"
#include "component/rigid_body_component.h"
#include "component/rotation_component.h"
#include "component/size_component.h"
#include "vec.h"

//GameObjectType is used when creating graphics objects so that all objects
//of one type have the same texture.
enum class GameObjectType{
    Default,
    Enemy,
    Wall,
    Floor
};



class GameObject
{
public:
    GameObjectType game_object_type = GameObjectType::Default;
    PositionComponent *position_component;
    SizeComponent *size_component;
    RotationComponent *rotation_component;
    RigidBodyComponent *rigid_body_component;
    Rectangle rect;

    GameObject(PositionComponent *p, SizeComponent *s,
        RotationComponent *rot, RigidBodyComponent *rig, GameObjectType t);
    GameObject();

    GameObject(const GameObject& go);
    void operator=(const GameObject& go);
    ~GameObject();
};

#endif
