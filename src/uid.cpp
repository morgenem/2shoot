#include "uid.h"

unsigned int UID::get_uid() const
{
    return uid;
}

void UID::set_uid(unsigned int u)
{
    uid = u;
}

bool UID::operator<(const UID &u) const
{
    return uid < u.uid;
}

bool UID::operator>(const UID &u) const
{
    return uid > u.uid;
}
