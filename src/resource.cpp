#include "resource.h"
#include <fstream>
#include <iterator>
#include <iostream>

Resource::Resource(std::string loc)
: file_location(loc)
{
}

bool Resource::open()
{
    std::ifstream file(file_location, std::ifstream::binary);

    if(!file.is_open())
        return false;

    file.seekg(0, file.end);
    const auto length = file.tellg();

    data.resize(length);

    file.seekg(0, file.beg);
    auto start = &*data.begin();
    file.read(start, length);
    file.close();
    
    return true;
}

const void *Resource::get_pointer() const
{
    return &data[0];
}

std::size_t Resource::get_size() const
{
    return data.size();
}
