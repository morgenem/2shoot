#ifndef UID_H
#define UID_H

class UID
{
protected:
    unsigned int uid;
public:
    unsigned int get_uid() const;

    void set_uid(unsigned int u);

    bool operator<(const UID &g) const;
    bool operator>(const UID &g) const;
};

#endif
