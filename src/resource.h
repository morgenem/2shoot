#ifndef RESOURCE_H
#define RESOURCE_H

#include <vector>
#include <string>

class Resource
{
    std::vector<char> data;
    std::string file_location;
public:
    Resource();
    Resource(std::string loc);

    bool open();
    const void *get_pointer() const;
    std::size_t get_size() const;
};

#endif
