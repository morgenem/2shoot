//Rectangle class
//Rectangle holds 4 position vecs which are the corner positions.
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "vec.h"
#include "typedef.h"
#include "component/size_component.h"

class Rectangle
{
public:
    PosVec ltop;
    PosVec rtop;
    PosVec lbot;
    PosVec rbot;
    Rectangle(PosVec lt, PosVec rt, PosVec lb, PosVec rb);
    Rectangle();

    SizeComponent size_component;

    PosVec get_center() const;

    static Rectangle from_two_corners(PosVec lt, PosVec rb); //Create a rectangle from 2 corners
    static Rectangle from_center(PosVec center, SizeVec size_vec); //Create from center and size
};

#endif
