#ifndef TYPEDEF_H
#define TYPEDEF_H

#include "vec.h"
#include "uid.h"

typedef Vec<float,2> PosVec; //Position Vector
typedef Vec<float,2> SizeVec;

typedef UID GraphicsObjectUID;
typedef UID GameObjectUID;

#endif
