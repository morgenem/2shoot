#ifndef BASE_UID_H
#define BASE_UID_H

class BaseUID
{
protected:
    unsigned int uid;
public:
    unsigned int get_uid() const;
    void set_uid(unsigned int u);

    bool operator<(const BaseUID &b) const;
    bool operator>(const BaseUID &b) const;
};

#endif
