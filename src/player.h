#ifndef PLAYER_H
#define PLAYER_H

#include "component/rigid_body_component.h"
#include "component/player_controller_component.h"
#include "component/position_component.h"
#include "component/rotation_component.h"
#include "typedef.h"

class Player
{
    float velocity;
public:
    Player(PosVec pos, SizeVec size_vec, float mass=70);
    RigidBodyComponent rigid_body_component;
    PositionComponent position_component;
    RotationComponent rotation_component;

    PlayerControllerComponent player_controller_component;

    void update();
};

#endif
