#include "game_object.h"

GameObject::GameObject(PositionComponent *p, SizeComponent *s,
    RotationComponent *rot, RigidBodyComponent *rig, GameObjectType t)
{
        position_component = p;
        size_component = s;
        rotation_component = rot;
        rigid_body_component = rig;

        rect = Rectangle::from_center(position_component->get_position(),
            size_component->get_size());

        game_object_type = t;
}

GameObject::GameObject()
{

}

GameObject::GameObject(const GameObject& go)
{
    position_component = new PositionComponent(*(go.position_component));
    size_component = new SizeComponent(*(go.size_component));
    rotation_component = new RotationComponent(*(go.rotation_component));

    rigid_body_component = new RigidBodyComponent(position_component, size_component->get_size());
    rigid_body_component->set_no_collision(go.rigid_body_component->get_no_collision());

    rect = go.rect;
    game_object_type = go.game_object_type;
}

void GameObject::operator=(const GameObject& go)
{
    position_component = new PositionComponent(*(go.position_component));
    size_component = new SizeComponent(*(go.size_component));
    rotation_component = new RotationComponent(*(go.rotation_component));

    //RigidBodyComponent is not copied, so we copy the no_collision bool manually here
    rigid_body_component = new RigidBodyComponent(position_component, size_component->get_size());
    rigid_body_component->set_no_collision(go.rigid_body_component->get_no_collision());

    rect = go.rect;
    game_object_type = go.game_object_type;
}

GameObject::~GameObject()
{
    delete position_component;
    delete size_component;
    delete rotation_component;
    delete rigid_body_component;
}
