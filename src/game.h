//Main game class
//Holds the game loop
#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "graphics/graphics_engine.h"
#include "vec.h"
#include "math_vec.h"
#include "math.h"
#include "resource.h"
#include "graphics/camera.h"
#include "input_engine.h"
#include "floor.h"
#include "typedef.h"
#include "game_object.h"

#include <cmath>
#include <string>
#include <vector>
#include <map>
#include <memory>

#ifdef DEBUG
    #define WINDOW_TITLE "The Library Job Debug"
#endif
#ifndef DEBUG
    #define WINDOW_TITLE "The Library Job"
#endif

#define MOVE_SPEED 1


class Game
{
    std::string window_title = std::string(WINDOW_TITLE) + " " + std::string(BUILD_VERSION);
    Vec<int,2> window_resolution = Vec<int,2>({1280,720});
    Vec<int,3> window_bg_color = Vec<int,3>({0,120,200});
    GraphicsEngine graphics_engine;

    InputEngine input_engine;

    std::map<GameObjectUID, GameObject> game_objects;
    std::map<GameObjectUID, GraphicsObjectUID> graphics_objects;

    std::string player_texture_loc = "/home/morgen/Documents/Projects/2shoot/assets/player/player.png";
    std::string floor_texture_loc = "/home/morgen/Documents/Projects/2shoot/assets/floor/floor-brick/floor-brick.png";
    std::string wall_texture_loc = "/home/morgen/Documents/Projects/2shoot/assets/wall/wall-brick/wall-brick.png";

private:
    const GameObjectUID get_new_game_obj_uid();
    const GameObjectUID create_new_enemy(PosVec pos,
        SizeVec size_vec, float rot);
    const GameObjectUID create_new_wall(Rectangle &r);
    const GameObjectUID create_new_floor(Rectangle &r);
public:
    Game();
    ~Game();
    void run();
};

#endif
