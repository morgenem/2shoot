#include "input_engine.h"
#include <SFML/Graphics.hpp> //Needed for sf::Keyboard

InputEngine::InputEngine(const Window &window)
: window_ref(window), mouse_pos({0,0})
{
    sf::Vector2i mp = sf::Mouse::getPosition(window_ref.get_sf_window_ref());
    mouse_pos = Vec<int,2>({mp.x, mp.y});
}

void InputEngine::update()
{
    keyboard_w = sf::Keyboard::isKeyPressed(sf::Keyboard::W);
    keyboard_a = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
    keyboard_s = sf::Keyboard::isKeyPressed(sf::Keyboard::S);
    keyboard_d = sf::Keyboard::isKeyPressed(sf::Keyboard::D);

    sf::Vector2i mp = sf::Mouse::getPosition(window_ref.get_sf_window_ref());
    mouse_pos = Vec<int,2>({mp.x, mp.y});
}

bool InputEngine::get_keyboard_w() const
{
    return keyboard_w;
}

bool InputEngine::get_keyboard_a() const
{
    return keyboard_a;
}

bool InputEngine::get_keyboard_s() const
{
    return keyboard_s;
}

bool InputEngine::get_keyboard_d() const
{
    return keyboard_d;
}

Vec<int,2> InputEngine::get_mouse_pos() const
{
    return mouse_pos;
}
