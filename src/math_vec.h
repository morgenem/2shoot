//A two value vector which has a magnitude and angle.
//Useful for physics and angle calculation.
#ifndef MATH_VEC_H
#define MATH_VEC_H

#include "vec.h"

class MathVec{
    float x_value;
    float y_value;
    Vec<float, 2> vec;
    void update_vec();
    void update_x_y();
public:
    MathVec(float x, float y);
    MathVec();
    //Setters
    void set_x(float x);
    void set_y(float y);
    void set_magnitude(float m);
    void set_angle(float a);

    void normalize();

    //Getters
    float get_x() const;
    float get_y() const;
    float get_magnitude() const;
    float get_angle() const;
    Vec<float, 2> get_vec() const;
};

#endif
