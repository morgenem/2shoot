#include "math_vec.h"
#include "math.h"

MathVec::MathVec(float x, float y)
: x_value(x), y_value(y), vec({0,0})
{
    update_vec();
}

MathVec::MathVec()
{

}

void MathVec::set_x(float x)
{
    x_value = x;
    update_vec();
}

void MathVec::set_y(float y)
{
    y_value = y;
    update_vec();
}

void MathVec::set_magnitude(float m)
{
    vec = Vec<float, 2>({m, vec[1]});
    update_x_y();
}

void MathVec::set_angle(float a)
{
    vec = Vec<float, 2>({vec[0],a});
    update_x_y();
}

void MathVec::update_vec()
{
    float magnitude = std::sqrt((x_value*x_value)+(y_value*y_value));
    float angle = Math::rad_to_deg(std::atan2(y_value, x_value));
    vec = Vec<float, 2>({magnitude, angle});
}

void MathVec::update_x_y()
{
    x_value = vec[0]*std::cos(Math::deg_to_rad(vec[1]));
    y_value = vec[0]*std::sin(Math::deg_to_rad(vec[1]));
}

void MathVec::normalize()
{
    x_value /= vec[0];
    y_value /= vec[0];
}

float MathVec::get_x() const
{
    return x_value;
}

float MathVec::get_y() const
{
    return y_value;
}

float MathVec::get_magnitude() const
{
    return vec[0];
}

float MathVec::get_angle() const
{
    return vec[1];
}

Vec<float, 2 >MathVec::get_vec() const
{
    return vec;
}
