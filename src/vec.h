#ifndef VEC_H
#define VEC_H

#include <cstddef>
#include <initializer_list>
#include <vector>

template<class T, std::size_t size>
class Vec
{
    T data[size];
public:
    Vec();
    Vec(T in[size]);
    Vec(std::initializer_list<T> list);
    Vec(const Vec &v);
    T operator[](int i) const;
};

template<class T, std::size_t size>
Vec<T, size>::Vec()
{
}

template<class T, std::size_t size>
Vec<T, size>::Vec(T in[size])
{
    for(int i=0; i<size; i++){
        data[i] = in[i];
    }
}

template<class T, std::size_t size>
Vec<T,size>::Vec(std::initializer_list<T> list)
{
    std::vector<T> tmp(list);
    for(int i=0; i<tmp.size(); i++){
        data[i] = tmp[i];
    }
}

template<class T, std::size_t size>
Vec<T,size>::Vec(const Vec &v)
{
    for(int i=0; i<size; i++){
        data[i] = v.data[i];
    }
}

template<class T, std::size_t size>
T Vec<T,size>::operator[](int i) const
{
    return data[i];
}


#endif
