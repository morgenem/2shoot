#include "game.h"

Game::Game()
: graphics_engine(window_title, window_resolution, window_bg_color),
input_engine(graphics_engine.get_window_ref())
{

}

/*
    TODO: make a class of containers so this code isn't reused
*/
const GameObjectUID Game::get_new_game_obj_uid()
{
    GameObjectUID g_uid;
    uint uid = 0;
    if(!game_objects.empty()){
        g_uid.set_uid(uid);
        while(game_objects.count(g_uid) != 0){
            uid++;
            g_uid.set_uid(uid);
        }
    }

    return g_uid;
}

const GameObjectUID Game::create_new_enemy(PosVec pos,
    SizeVec size_vec, float rot)
{
    PositionComponent *p = new PositionComponent(pos);
    GameObject enemy(   p,
                        new SizeComponent(size_vec),
                        new RotationComponent(rot),
                        new RigidBodyComponent(p, size_vec),
                        GameObjectType::Enemy
                    );

    const GameObjectUID g_uid = get_new_game_obj_uid();
    game_objects[g_uid] = enemy;
    return g_uid;
}

const GameObjectUID Game::create_new_wall(Rectangle &r)
{
    PositionComponent *p = new PositionComponent(r.get_center());
    GameObject wall(    p,
                        new SizeComponent(r.size_component.get_size()),
                        new RotationComponent(0),
                        new RigidBodyComponent(p, r),
                        GameObjectType::Wall
                    );
    const GameObjectUID g_uid = get_new_game_obj_uid();
    game_objects[g_uid] = wall;
    return g_uid;
}

const GameObjectUID Game::create_new_floor(Rectangle &r)
{
    PositionComponent *p = new PositionComponent(r.get_center());
    RigidBodyComponent *rig = new RigidBodyComponent(p, r);
    rig->set_no_collision(true);
    GameObject floor(    p,
                        new SizeComponent(r.size_component.get_size()),
                        new RotationComponent(0),
                        rig,
                        GameObjectType::Floor
                    );
    const GameObjectUID g_uid = get_new_game_obj_uid();
    game_objects[g_uid] = floor;
    return g_uid;
}

void Game::run()
{

    Resource player_texture(player_texture_loc);
    if(!player_texture.open()){
        std::cout << "Error opening player texture png file.\n";
        return;
    }

    TextureUID tex = graphics_engine.create_new_texture(player_texture, SizeVec({128,128}));

    Player player(PosVec({20,20}), SizeVec({0.4,0.5}));
    GraphicsObjectUID g_player = graphics_engine.create_new_g_obj(
        player.position_component.get_position(),
        player.rotation_component.get_rotation(),
        SizeVec({50,50}),
        Vec<float,2>({58,64}),
        tex, 1
    );

    Camera camera(player.position_component, Vec<float,2>({1280,720}));

    const GameObjectUID enemy = create_new_enemy(PosVec({22,22}), SizeVec({0.4,0.5}), 0);

    //FLOOR
    PosVec ltop({15,25});
    PosVec rbot({25,15});
    Rectangle floor_rect = Rectangle::from_two_corners(ltop,rbot);

    Resource floor_texture(floor_texture_loc);
    if(!floor_texture.open()){
        std::cout << "Error opening floor texture png file.\n";
        return;
    }
    TextureUID floor_tex = graphics_engine.create_new_texture(floor_texture,
        Vec<float,2>({128,128}), true);
    const GameObjectUID floor = create_new_floor(floor_rect);

    //WALL
    {
        ltop = PosVec({14, 25}); //Reusing the ltop and rbot from floor_rect
        rbot = PosVec({15,15});
        Rectangle wall_rect = Rectangle::from_two_corners(ltop,rbot);
        GameObjectUID wall = create_new_wall(wall_rect);

        ltop = PosVec({14,26});
        rbot = PosVec({26,25});
        Rectangle wall_rect_2 = Rectangle::from_two_corners(ltop,rbot);
        create_new_wall(wall_rect_2);

        ltop = PosVec({25,26});
        rbot = PosVec({26,15});
        Rectangle wall_rect_3 = Rectangle::from_two_corners(ltop,rbot);
        create_new_wall(wall_rect_3);

        ltop = PosVec({14,15});
        rbot = PosVec({26,14});
        Rectangle wall_rect_4 = Rectangle::from_two_corners(ltop,rbot);
        create_new_wall(wall_rect_4);
    }

    Resource wall_texture(wall_texture_loc);
    if(!wall_texture.open()){
        std::cout << "Error opening wall texture png file.\n";
        return;
    }
    TextureUID wall_tex = graphics_engine.create_new_texture(wall_texture,
        SizeVec({128,128}), true);

    for(auto it=game_objects.begin(); it!=game_objects.end(); it++){
        uint p = 0;
        TextureUID t;
        bool use_texture_rect = false;
        Vec<float,2> origin({0,0});
        Rectangle rect = it->second.rect;
        switch(it->second.game_object_type){
            case GameObjectType::Enemy:
                p = 1;
                t = tex;
                origin = Vec<float,2>({58,64});
                rect = Rectangle::from_center(
                    rect.get_center(),
                    SizeVec({1,1})
                );
                break;
            case GameObjectType::Wall:
                p = 0;
                t = wall_tex;
                use_texture_rect = true;
                break;
            case GameObjectType::Floor:
                p = 0;
                t = floor_tex;
                use_texture_rect = true;
                break;
            default:
                p = 1;
                t = tex;
                std::cout << "Error creating graphics_object. Defaulting to enemy tex.\n";
                break;
        }
        graphics_objects[it->first] = graphics_engine.create_new_g_obj(
            camera.get_screen_rect(rect),
            it->second.rotation_component->get_rotation(),
            t,
            p,
            origin,
            use_texture_rect
        );
    }

    //lambda function. & captures by reference
    auto check_player_collision = [&](MathVec np)
    {
        const float distance_const = 2.0; //Distance when collision detection is starting to be checked
        PosVec p = player.position_component.get_position();
        for(auto it=game_objects.begin(); it!=game_objects.end(); it++){
            PosVec g = it->second.position_component->get_position();
            if(Math::abs(p[0]-g[0])<distance_const+it->second.size_component->get_size()[0] &&
                Math::abs(p[1]-g[1])<distance_const+it->second.size_component->get_size()[1])
            {
                if(!it->second.rigid_body_component->get_no_collision()){ //If collision is activated
                    if(player.player_controller_component.check_collision(np, it->second.rigid_body_component))
                        return true;
                }
            }
        }
        return false;
    };

    while(graphics_engine.get_running()){
        graphics_engine.poll_window();
        if(!graphics_engine.get_running())
            break;
        input_engine.update();

        if(input_engine.get_keyboard_w()){
            if(!check_player_collision(MathVec(0,1)))
                player.player_controller_component.move(MathVec(0,1));
        }
        if(input_engine.get_keyboard_a()){
            if(!check_player_collision(MathVec(-1,0)))
                player.player_controller_component.move(MathVec(-1,0));
        }
        if(input_engine.get_keyboard_s()){
            if(!check_player_collision(MathVec(0,-1)))
                player.player_controller_component.move(MathVec(0,-1));
        }
        if(input_engine.get_keyboard_d()){
            if(!check_player_collision(MathVec(1,0)))
                player.player_controller_component.move(MathVec(1,0));
        }

        {
            Vec<int,2> mouse_pos = input_engine.get_mouse_pos();
            PosVec player_pos = graphics_engine.get_g_obj_pos(g_player);

            //Straight line from the mouse to the player
            Vec<float, 2> mouse_dist({mouse_pos[0]-player_pos[0], mouse_pos[1]-player_pos[1]});
            float mouse_angle = Math::rad_to_deg(std::atan2(mouse_dist[1], mouse_dist[0]));
            player.rotation_component.set_rotation(mouse_angle);
        }

        player.update();

        //graphics_engine.update_g_obj_rect(g_floor,
            //camera.get_screen_rect(floor.rect));
        for(auto it=graphics_objects.begin(); it!=graphics_objects.end(); it++){
            graphics_engine.update_g_obj_rect(
                it->second,
                camera.get_screen_rect(game_objects[it->first].rect)
            );
        }
        /*graphics_engine.update_g_obj_rect(
            g_wall,
            camera.get_screen_rect(game_objects[wall].rect)
        );
        graphics_engine.update_g_obj_rect(
            g_enemy,
            camera.get_screen_rect(game_objects[enemy].rect)
        );*/

        graphics_engine.update_g_obj_pos(g_player,
           camera.get_screen_pos(player.position_component.get_position()));
        graphics_engine.update_g_obj_rot(g_player,
            player.rotation_component.get_rotation());

        graphics_engine.update();
    }
}

Game::~Game()
{
}
