#include "camera.h"

Camera::Camera(PositionComponent& pos, SizeVec window_size_in)
: position_component_linked(pos), window_size({window_size_in[0], window_size_in[1]})
{
    pixels_per_meter_x = 50;
    pixels_per_meter_y = (window_size[1]/window_size[0]) * 50;
}

void Camera::link_position_component(PositionComponent& pos)
{
    position_component_linked = pos;
}

Vec<float,2> Camera::get_screen_pos(PosVec pos)
{
    //Get distance to camera
    PosVec cam_pos = position_component_linked.get_position();
    float dist_to_camera_x = cam_pos[0]-pos[0];
    float dist_to_camera_y = cam_pos[1]-pos[1];

    //Convert to screen units using pixels_per_meter
    dist_to_camera_x *= pixels_per_meter_x;
    dist_to_camera_y *= pixels_per_meter_x; //Does SFML do this for us? Because
                                            //when I calculate the pixels I get problems
                                            //But it's perfect like this.

    float position_x = window_size[0]/2 - dist_to_camera_x;
    float position_y = window_size[1]/2 - dist_to_camera_y;

    return PosVec({position_x, position_y});
}

Rectangle Camera::get_screen_rect(Rectangle rect)
{
    PosVec lt = get_screen_pos(rect.ltop);
    PosVec rt = get_screen_pos(rect.rtop);
    PosVec lb = get_screen_pos(rect.lbot);
    PosVec rb = get_screen_pos(rect.rbot);

    return Rectangle(lt,rt,lb,rb);
}
