#include "graphics_object.h"

GraphicsObject::GraphicsObject(PosVec pos, float rot, SizeVec size,
    Vec<float,2> origin, Texture& tex, GraphicsObjectUID g, uint priority_in)
:   position_component(pos), g_uid(g), rotation_component(rot),
    size_component(size), sprite(tex), priority(priority_in)
{
    SizeVec s = size_component.get_size();

    sprite.set_size(SizeVec({50,50}));
    int x_rect = (s[0]/50) * 128; //Multiply the size of obj in tiles by tex size
    int y_rect = (s[1]/50) * 128;
    sprite.set_texture_rect(Vec<int,2>({x_rect, y_rect}));
    sprite.set_origin(origin);
}

GraphicsObject::GraphicsObject(Rectangle rect, float rot,
    Texture &tex, GraphicsObjectUID g, uint priority_in, Vec<float,2> origin, bool use_texture_rect)
:   sprite(tex), position_component(rect.lbot), rotation_component(rot), priority(priority_in),
    size_component(rect.size_component.get_size())
{

    SizeVec s = size_component.get_size();

    sprite.set_size(SizeVec({50,50}));
    int x_rect = (s[0]/50) * 128; //Multiply the size of obj in tiles by tex size
    int y_rect = (s[1]/50) * 128;
    sprite.set_texture_rect(Vec<int,2>({x_rect, y_rect}));
    sprite.set_origin(origin);
}

GraphicsObject::GraphicsObject(const GraphicsObject& g)
: position_component(g.position_component), rotation_component(g.rotation_component),
    size_component(g.size_component), priority(g.priority)
{
}

GraphicsObject::GraphicsObject()
: position_component(PosVec({0,0})), rotation_component(90.0f),
    size_component(SizeVec({20,20}))
{
}

void GraphicsObject::update()
{
    sprite.set_position(position_component.get_position());
    sprite.set_rotation(rotation_component.get_rotation());
}

const Sprite& GraphicsObject::get_sprite() const
{
    return sprite;
}

uint GraphicsObject::get_priority() const
{
    return priority;
}
