//Texture class
//SFML specific functionality. Each sprite has a reference to a texture.

#ifndef TEXTURE_H
#define TEXTURE_H

#include <SFML/Graphics.hpp>
#include "../resource.h"

#include "../component/size_component.h"
#include "../typedef.h"

class Texture
{
    sf::Texture sf_texture;
    bool is_tile;

public:
    Texture(Resource texture_data, SizeVec size, bool tile=false);
    Texture();
    const sf::Texture& get_sf_texture() const;

    SizeComponent size_component;
};

#endif
