#ifndef GRAPHICS_OBJECT_H
#define GRAPHICS_OBJECT_H

#include "../component/position_component.h"
#include "../component/rotation_component.h"
#include "../component/size_component.h"
#include "../vec.h"
#include "../rectangle.h"
#include "graphics_object_uid.h"
#include "sprite.h"
#include "../typedef.h"

class GraphicsObject
{
    GraphicsObjectUID g_uid;
    Sprite sprite;
    uint priority;

public:
    GraphicsObject(PosVec pos, float rot, SizeVec size,
        Vec<float,2> origin, Texture& tex, GraphicsObjectUID g, uint priority_in);
    GraphicsObject(Rectangle rect, float rot, Texture& tex,
        GraphicsObjectUID g, uint priority_in, Vec<float,2> origin=Vec<float,2>({0,0}),
        bool use_texture_rect=false);
    GraphicsObject(const GraphicsObject& g);
    GraphicsObject();

    PositionComponent position_component; //Position on screen
    RotationComponent rotation_component;
    SizeComponent size_component;

    void update();

    const Sprite& get_sprite() const;
    uint get_priority() const;
};

#endif
