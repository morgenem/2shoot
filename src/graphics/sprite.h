//Sprite class
//SFML specific functionality. Sprite is an instance of a drawable object in SFML.
//Each graphics object has its own sprite to be drawn with SFML.

#ifndef SPRITE_H
#define SPRITE_H

#include <SFML/Graphics.hpp>
#include "texture.h"
#include "../typedef.h"
#include "../component/size_component.h"

class Sprite
{
    sf::Sprite sf_sprite;
    Vec<float,2> original_size;

public:
    Sprite();
    Sprite(const Texture &tex);

    void set_size(SizeVec size);
    void set_origin(Vec<float,2> origin);
    void set_position(PosVec pos);
    void set_rotation(float rot);
    void set_texture_rect(Vec<int,2> rect_value);

    sf::Sprite get_sf_sprite() const;

    SizeComponent size_component;
};

#endif
