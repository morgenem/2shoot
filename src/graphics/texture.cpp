#include "texture.h"
#include <iostream>

Texture::Texture()
: size_component(SizeVec{20,20}), is_tile(false)
{

}

Texture::Texture(Resource texture_data, SizeVec size, bool tile)
: size_component(size)
{
    if(!sf_texture.loadFromMemory(texture_data.get_pointer(), texture_data.get_size()))
        std::cout << "Texture load from memory failure\n";
    is_tile = tile;
    sf_texture.setRepeated(is_tile);
}

const sf::Texture& Texture::get_sf_texture() const
{
    return sf_texture;
}
