#ifndef GRAPHICS_ENGINE_H
#define GRAPHICS_ENGINE_H

#include "window.h"
#include "graphics_object.h"
#include "../vec.h"
#include "texture.h"
#include "texture_uid.h"
#include "../rectangle.h"
#include "../typedef.h"
#include <map>

class GraphicsEngine
{
    Window window;
    bool running;
    std::map<GraphicsObjectUID, GraphicsObject> g_objs;
    std::map<TextureUID, Texture> textures;
    Vec<int, 2> window_size_vec;
public:
    GraphicsEngine(std::string window_name,
        Vec<int, 2> window_size, Vec<int, 3> window_color);
    bool get_running() const;
    void poll_window();

    void update_g_obj_pos(GraphicsObjectUID g, PosVec g_position);
    void update_g_obj_rot(GraphicsObjectUID g, float rot);
    void update_g_obj_rect(GraphicsObjectUID g, Rectangle rect);

    PosVec get_g_obj_pos(GraphicsObjectUID g);

    void update();
    const TextureUID create_new_texture(Resource tex_resource, SizeVec size,
        bool tile=false);
    const GraphicsObjectUID create_new_g_obj(PosVec g_position,
        float rot, SizeVec size, Vec<float,2> origin, TextureUID tex,
        uint priority);
    const GraphicsObjectUID create_new_g_obj(Rectangle rect, float rot,
        TextureUID tex, uint priority, Vec<float,2> origin=Vec<float,2>({0,0}),
        bool use_texture_rect=false);

    const Window& get_window_ref() const;
};

#endif
