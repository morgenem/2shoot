//Camera class
//Used to convert game world positions to screen positions.
//Camera follows the player around with link_position_component, but
//changing it would not be difficult.

#ifndef CAMERA_H
#define CAMERA_H

#include "../component/position_component.h"
#include "../typedef.h"
#include "../rectangle.h"

class Camera
{
    float pixels_per_meter_x;
    float pixels_per_meter_y;
    SizeVec window_size;
public:
    Camera(PositionComponent& pos, SizeVec window_size_in);

    void link_position_component(PositionComponent& pos);

    PosVec get_screen_pos(PosVec pos); //Create screen pos from game world pos
    Rectangle get_screen_rect(Rectangle rec); //Create a rect of screen pos from a game world rect

    PositionComponent& position_component_linked;
};

#endif
