#include "graphics_engine.h"

GraphicsEngine::GraphicsEngine(std::string window_name,
    Vec<int, 2> window_size, Vec<int, 3> window_color)
: window(window_name, window_size, window_color), running(true), window_size_vec(window_size)
{

}

bool GraphicsEngine::get_running() const
{
    return running;
}

void GraphicsEngine::poll_window()
{
    window.poll_event();
    if(!window.get_open()){
        running = false;
        return;
    }
}

void GraphicsEngine::update_g_obj_pos(GraphicsObjectUID g, PosVec position)
{
    //Y position is subtracted from window_size y as SFML draws from the top left
    //instead of the bottom left like any sane person would.
    PosVec g_pos({position[0], window_size_vec[1]-position[1]});
    g_objs[g].position_component.set_position(g_pos);
}

void GraphicsEngine::update_g_obj_rot(GraphicsObjectUID g, float rot)
{
    g_objs[g].rotation_component.set_rotation(rot);
}

void GraphicsEngine::update_g_obj_rect(GraphicsObjectUID g, Rectangle rect)
{
    PosVec g_lbot({rect.ltop[0], window_size_vec[1]-rect.ltop[1]});
    g_objs[g].position_component.set_position(g_lbot);
}

PosVec GraphicsEngine::get_g_obj_pos(GraphicsObjectUID g)
{
    return g_objs[g].position_component.get_position();
}

void GraphicsEngine::update()
{
    window.clear();
    std::map<GraphicsObjectUID, GraphicsObject>::iterator it = g_objs.begin();
    while(it != g_objs.end()){ //First priority draw
        if(it->second.get_priority() == 0){
            it->second.update();
            window.draw_graphics_object(it->second);
        }
        it++;
    }
    it = g_objs.begin();
    while(it != g_objs.end()){ //Second priority draw
        if(it->second.get_priority() == 1){
            it->second.update();
            window.draw_graphics_object(it->second);
        }
        it++;
    }
    window.display();
}

const TextureUID GraphicsEngine::create_new_texture(Resource tex_resource,
    SizeVec size, bool tile)
{
    TextureUID t_uid;
    uint uid = 0;
    if(!textures.empty()){
        TextureUID tmp;
        tmp.set_uid(uid);
        while(textures.count(tmp) != 0){
            uid++;
            tmp.set_uid(uid);
        }
    }
    t_uid.set_uid(uid);

    Texture tex(tex_resource, size, tile);
    textures[t_uid] = tex;
    return t_uid;
}

const GraphicsObjectUID GraphicsEngine::create_new_g_obj(PosVec position,
    float rot, SizeVec size, Vec<float,2> origin, TextureUID tex,
    uint priority)
{
    GraphicsObjectUID g_uid;
    uint uid = 0;
    if(!g_objs.empty()){
        g_uid.set_uid(uid);
        while(g_objs.count(g_uid) != 0){
            uid++;
            g_uid.set_uid(uid);
        }
    }
    PosVec g_pos({position[0], position[1]}); //Why?
    g_objs[g_uid] = GraphicsObject(g_pos, rot, size, origin, textures[tex], g_uid,
        priority);
    return g_uid;
}

const GraphicsObjectUID GraphicsEngine::create_new_g_obj(Rectangle rect, float rot,
    TextureUID tex, uint priority, Vec<float,2> origin, bool use_texture_rect)
{
    GraphicsObjectUID g_uid;
    uint uid = 0;
    if(!g_objs.empty()){
        g_uid.set_uid(uid);
        while(g_objs.count(g_uid) != 0){
            uid++;
            g_uid.set_uid(uid);
        }
    }
    g_objs[g_uid] = GraphicsObject(rect, rot, textures[tex], g_uid, priority, origin, use_texture_rect);
    return g_uid;
}

const Window& GraphicsEngine::get_window_ref() const
{
    return window;
}
