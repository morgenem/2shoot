#ifndef WINDOW_H
#define WINDOW_H

#include <SFML/Graphics.hpp>
#include <string>
#include "../typedef.h"
#include "graphics_object.h"

class Window
{
    sf::RenderWindow sf_window;
    sf::Color clear_color;
    bool open;

public:
    Window(std::string window_name, Vec<int, 2> size, Vec<int, 3> color);
    bool get_open() const;
    void poll_event();
    void clear();
    void display();
    //void draw_rect(PosVec location, float rot, Vec<int,2> size);
    void draw_graphics_object(GraphicsObject& g_obj);

    const sf::RenderWindow& get_sf_window_ref() const;
};

#endif
