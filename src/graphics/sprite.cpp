#include "sprite.h"

Sprite::Sprite()
: size_component(SizeVec({20,20}))
{
    original_size = SizeVec({20,20});
}

Sprite::Sprite(const Texture &tex)
: size_component(tex.size_component.get_size())
{
    original_size = tex.size_component.get_size();
    sf_sprite.setTexture(tex.get_sf_texture());
}

void Sprite::set_size(SizeVec size)
{
    float new_size_x = size[0]/original_size[0];
    float new_size_y = size[1]/original_size[1];
    sf_sprite.setScale(new_size_x, new_size_y);
    size_component.set_size(SizeVec({new_size_x, new_size_y}));
}

void Sprite::set_origin(Vec<float,2> origin)
{
    sf_sprite.setOrigin(origin[0], origin[1]);
}

void Sprite::set_position(PosVec pos)
{
    sf_sprite.setPosition(pos[0], pos[1]);
}

void Sprite::set_rotation(float rot)
{
    sf_sprite.setRotation(rot);
}

void Sprite::set_texture_rect(Vec<int,2> rect_value)
{
    sf_sprite.setTextureRect({0, 0, rect_value[0], rect_value[1]});
}

sf::Sprite Sprite::get_sf_sprite() const
{
    return sf_sprite;
}
