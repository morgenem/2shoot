#include "window.h"

Window::Window(std::string window_name, Vec<int, 2> size, Vec<int, 3> color)
: sf_window(sf::VideoMode(size[0], size[1]), window_name), open(true),
clear_color(color[0], color[1], color[2])
{
    sf_window.setVerticalSyncEnabled(true);
    sf_window.setFramerateLimit(60);
}

bool Window::get_open() const
{
    return open;
}

void Window::poll_event()
{
    sf::Event event;
    while(sf_window.pollEvent(event)){
        if(event.type == sf::Event::Closed){
            open = false;
            sf_window.close();
        }
    }
}

void Window::clear()
{
    sf_window.clear(clear_color);
}

void Window::display()
{
    sf_window.display();
}

/*void Window::draw_rect(Vec<float,2> position, float rot, Vec<int,2> size)
{
    sf::RectangleShape rect(sf::Vector2f(size[0],size[1]));
    rect.setPosition(sf::Vector2f(position[0],position[1]));
    rect.setRotation(rot);
    rect.setOrigin(size[0]/2, size[1]/2);
    sf_window.draw(rect);
}*/

void Window::draw_graphics_object(GraphicsObject& g_obj)
{
    sf_window.draw(g_obj.get_sprite().get_sf_sprite());
}

const sf::RenderWindow& Window::get_sf_window_ref() const
{
    return sf_window;
}
