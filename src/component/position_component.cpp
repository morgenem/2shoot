#include "position_component.h"

PositionComponent::PositionComponent()
{
}

PositionComponent::PositionComponent(PosVec p)
: position_vec({p[0], p[1]})
{

}

PositionComponent::PositionComponent(const PositionComponent& p)
: position_vec(p.position_vec)
{

}

PosVec PositionComponent::get_position() const
{
    return position_vec;
}

void PositionComponent::set_position(PosVec p)
{
    position_vec = p;
}
