#ifndef ROTATION_COMPONENT_H
#define ROTATION_COMPONENT_H

#include "component.h"

class RotationComponent: public Component{
    float rotation;
public:
    RotationComponent();
    RotationComponent(const RotationComponent& r);
    RotationComponent(float rot);
    float get_rotation() const;

    void set_rotation(float rot);
};

#endif
