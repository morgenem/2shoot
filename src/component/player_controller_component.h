#ifndef PLAYER_CONTROLLER_COMPONENT_H
#define PLAYER_CONTROLLER_COMPONENT_H

#include "component.h"
#include "rigid_body_component.h"
#include "../math_vec.h"
#include "../typedef.h"
#include "position_component.h"

class PlayerControllerComponent : public Component
{
    PositionComponent &position_component;
    RigidBodyComponent &rigid_body_component;
    float velocity;
    PosVec create_new_pos_vec(MathVec move_vec);
public:
    PlayerControllerComponent(PositionComponent &pos, RigidBodyComponent &rigid,
        float vel);
    void move(MathVec move_vec);
    bool check_collision(MathVec move_vec, const RigidBodyComponent* body);
};

#endif
