#include "player_controller_component.h"

#include "../vec.h"

PlayerControllerComponent::PlayerControllerComponent(PositionComponent &pos,
    RigidBodyComponent &rigid, float vel)
: position_component(pos), rigid_body_component(rigid), velocity(vel)
{

}

PosVec PlayerControllerComponent::create_new_pos_vec(MathVec move_vec)
{
    move_vec.normalize();
    float new_x  = position_component.get_position()[0] + (move_vec.get_x()*velocity);
    float new_y = position_component.get_position()[1] + (move_vec.get_y()*velocity);
    return PosVec({new_x,new_y});
}

void PlayerControllerComponent::move(MathVec move_vec)
{
    PosVec new_pos = create_new_pos_vec(move_vec);
    position_component.set_position(new_pos);
}

bool PlayerControllerComponent::check_collision(
    MathVec move_vec, const RigidBodyComponent* body)
{
    PosVec new_pos = create_new_pos_vec(move_vec);

    return rigid_body_component.get_collision(new_pos, body);
}
