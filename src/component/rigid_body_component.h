//Rigid body Component
//Handles some physics and collisions
//Much of this file is commented out because it is not in use yet
//However it may be used later for the movement of non-character objects

#ifndef RIGID_BODY_COMPONENT_H
#define RIGID_BODY_COMPONENT_H

#include "component.h"
#include "position_component.h"
#include "size_component.h"
#include "../vec.h"
#include "../math_vec.h"
#include "../rectangle.h"
#include "../typedef.h"

#include <vector>
#include <memory>

class RigidBodyComponent: public Component
{
    MathVec current_force;
    MathVec current_velocity;
    float current_max_velocity; //Limit to velocity
    float mass;
    std::vector<MathVec> friction_forces;
    PositionComponent *position_component_ptr;

    Rectangle collision_rect;
    const float collision_size_x = 0.1; //Increase the collision size
    const float collision_size_y = 0.1;

    bool no_collision = false;

private:
    void init_rect(Rectangle &r);
    void init_pos(SizeVec size_vec);
public:
    SizeComponent size_component;
    RigidBodyComponent(PositionComponent *p, Rectangle &r);
    RigidBodyComponent(PositionComponent *p, SizeVec size_vec);

    void set_no_collision(bool c);
    bool get_no_collision() const;

    /*
    void add_force(MathVec vec); //Magnitude and angle
    void add_friction_force(MathVec vec);

    void zero_force(); //Removes force
    void zero_velocity(); //Removes velocity
    void set_velocity(MathVec v);

    void set_max_velocity(float magnitude);
    */
    void update();

    /*
    MathVec get_current_force() const;
    float get_mass() const;
    */

    bool get_collision(PosVec next_pos, const RigidBodyComponent* body) const;
};

#endif
