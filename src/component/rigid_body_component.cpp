//Much of this file is commented out because it is not in use yet.
//However it may be used later for the movement of non-character objects.

#include "rigid_body_component.h"
#include <cmath>

RigidBodyComponent::RigidBodyComponent(PositionComponent *p, Rectangle &r)
:   position_component_ptr(p)
{
    init_rect(r);
}

RigidBodyComponent::RigidBodyComponent(PositionComponent *p, SizeVec size_vec)
:   position_component_ptr(p)
{
    init_pos(size_vec);
}

void RigidBodyComponent::init_rect(Rectangle &r)
{
    size_component = r.size_component;
    collision_rect = r;
}

void RigidBodyComponent::init_pos(SizeVec size_vec)
{
    size_component = SizeComponent(size_vec);

    collision_rect = Rectangle::from_center(
            position_component_ptr->get_position(), size_component.get_size()
    );
}

void RigidBodyComponent::set_no_collision(bool c)
{
    no_collision = c;
}

bool RigidBodyComponent::get_no_collision() const
{
    return no_collision;
}

/*
void RigidBodyComponent::add_force(MathVec vec)
{
    float force_x = current_force.get_x() + vec.get_x();
    float force_y = current_force.get_y() + vec.get_y();
    MathVec v(force_x, force_y);
    current_force = v;
}

void RigidBodyComponent::add_friction_force(MathVec vec)
{
    friction_forces.push_back(vec);
}

void RigidBodyComponent::zero_force()
{
    current_force = MathVec(0,0);
}

void RigidBodyComponent::zero_velocity()
{
    current_velocity = MathVec(0,0);
}

void RigidBodyComponent::set_velocity(MathVec v)
{
    current_velocity = v;
}

void RigidBodyComponent::set_max_velocity(float magnitude)
{
    current_max_velocity = magnitude;
}
*/
void RigidBodyComponent::update()
{
    /*//Handle friction_forces
    for(size_t i=0; i<friction_forces.size(); i++){
        if((friction_forces[i].get_x() > 0 && current_velocity.get_x() < 0)
            || (friction_forces[i].get_x() < 0 && current_velocity.get_x() > 0)){
            current_force = MathVec(
                current_force.get_x()+friction_forces[i].get_x(), current_force.get_y());
        }
        else{
            current_velocity.set_x(0);
        }
        if((friction_forces[i].get_y() > 0 && current_velocity.get_y() < 0)
            || (friction_forces[i].get_y() < 0 && current_velocity.get_y() > 0)){
            current_force = MathVec(
                current_force.get_x(), current_force.get_y()+friction_forces[i].get_y());
        }
        else{
            current_velocity.set_y(0);
        }
        if(current_velocity.get_x() == 0 && current_velocity.get_y() == 0){
            friction_forces.clear();
            zero_force();
            break;
        }
    }

    //Update velocity based on acceleration
    if(current_force.get_magnitude() != 0){
        float new_x_velocity = current_velocity.get_x() + (current_force.get_x())/mass;

        float new_y_velocity = current_velocity.get_y() +  (current_force.get_y())/mass;

        MathVec new_velocity(new_x_velocity, new_y_velocity);
        current_velocity = new_velocity;
    }
    if(current_velocity.get_magnitude() >= current_max_velocity){
        current_velocity.set_magnitude(current_max_velocity);
        zero_force();
    }

    //Move based on velocity
    if(current_velocity.get_magnitude() != 0){
        Vec<float, 2> pvec = position_component.get_position();
        float new_x = pvec[0] + current_velocity.get_x();
        float new_y = pvec[1] + current_velocity.get_y();
        position_component.set_position(Vec<float,2>({new_x,new_y}));
    }*/

    collision_rect = Rectangle::from_center(
        position_component_ptr->get_position(), size_component.get_size()
    );
}

/*
MathVec RigidBodyComponent::get_current_force() const
{
    return current_force;
}

float RigidBodyComponent::get_mass() const
{
    return mass;
}
*/
bool RigidBodyComponent::get_collision(PosVec next_pos,
    const RigidBodyComponent* body) const
{
    /*if(body->no_collision == true || no_collision == true){
        return false;
    }*/

    Rectangle np_collision_rect = Rectangle::from_center(
        next_pos, size_component.get_size()
    );

    float np_right = np_collision_rect.rtop[0]+collision_size_x;
    float np_left = np_collision_rect.ltop[0]-collision_size_x;
    float np_top = np_collision_rect.rtop[1]+collision_size_y;
    float np_bot = np_collision_rect.rbot[1]-collision_size_y;

    float b_right = body->collision_rect.rtop[0]+collision_size_x;
    float b_left = body->collision_rect.ltop[0]-collision_size_x;
    float b_top = body->collision_rect.rtop[1]+collision_size_y;
    float b_bot = body->collision_rect.rbot[1]-collision_size_y;

    if(np_right >= b_left && np_left <= b_right){
        if(np_top >= b_bot && np_bot <= b_top){
            return true;
        }
    }

    return false;
}
