#include "size_component.h"

SizeComponent::SizeComponent()
{
    size = SizeVec({0,0});
}

SizeComponent::SizeComponent(const SizeComponent& s)
{
    size = s.size;
}

SizeComponent::SizeComponent(SizeVec in_size)
: size({in_size[0], in_size[1]})
{
}

void SizeComponent::set_size(SizeVec in_size)
{
    size = in_size;
}

SizeVec SizeComponent::get_size() const
{
    return size;
}
