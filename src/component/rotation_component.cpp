#include "rotation_component.h"

RotationComponent::RotationComponent()
{

}

RotationComponent::RotationComponent(const RotationComponent& r)
{
    rotation = r.rotation;
}

RotationComponent::RotationComponent(float rot)
{
    rotation = rot;
}

float RotationComponent::get_rotation() const
{
    return rotation;
}

void RotationComponent::set_rotation(float rot)
{
    rotation = rot;
}
