#ifndef POSITION_COMPONENT_H
#define POSITION_COMPONENT_H

#include "component.h"
#include "../vec.h"
#include "../typedef.h"

class PositionComponent: public Component
{
    PosVec position_vec;
public:
    PositionComponent();
    PositionComponent(PosVec p);
    PositionComponent(const PositionComponent& p);
    PosVec get_position() const;
    void set_position(PosVec p);
};

#endif
