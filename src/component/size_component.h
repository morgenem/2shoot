#ifndef SIZE_COMPONENT_H
#define SIZE_COMPONENT_H

#include "component.h"
#include "../typedef.h"

class SizeComponent: public Component
{
    Vec<float,2> size;

public:
    SizeComponent(SizeVec in_size);
    SizeComponent();
    SizeComponent(const SizeComponent& s);

    void set_size(SizeVec in_size);

    SizeVec get_size() const;
};

#endif
