#include "base_uid.h"

unsigned int BaseUID::get_uid() const
{
    return uid;
}

void BaseUID::set_uid(unsigned int u)
{
    uid = u;
}

bool BaseUID::operator<(const BaseUID &g) const
{
    return uid < g.uid;
}
bool BaseUID::operator>(const BaseUID &g) const
{
    return uid > g.uid;
}
